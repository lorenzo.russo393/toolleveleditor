using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteAlways]
public class Room : MonoBehaviour
{
    public int totalDoors;
    public int currentDoorIndex = 0;

    public Collider currentDoor;
    public Collider collidedDoor;

    public bool draw = true;

    private void OnEnable()
    {
        transform.rotation = new Quaternion(0,0,0,0);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
    }
}
