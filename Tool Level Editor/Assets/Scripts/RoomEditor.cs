using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.XR;
using UnityEngine;

public class RoomEditor : EditorWindow
{
    public static event Action<int> OnRoomRotated;

    public static bool isInPrefabPreviewe;

    public bool spawned = false;

    public float xRect;
    public float yRect;
    public float radius;

    public Ray ray;
    public RaycastHit hit;
    public Quaternion rotation;
    public float y;

    SerializedObject serializedObject;
    SerializedProperty xRectProperty;
    SerializedProperty yRectProperty;
    SerializedProperty radiusProperty;

    public GameObject[] prefabs;
    bool[] selectedPrefab;
    public GameObject prefabToSpawn;

    GameObject obj = null;

    Quaternion rot;
    public static bool anyDoorFounded = false;

    Vector3 posToSpawn;
    int timeRotated = 0;

    [MenuItem("Tools/Room Selection")]
    public static void ShowWindow() => EditorWindow.GetWindow(typeof(RoomEditor), false, "Room Selection");

    private void OnEnable()
    {
        posToSpawn = Vector3.zero;
        //rot = Quaternion.identity;
        serializedObject = new SerializedObject(this);

        xRectProperty = serializedObject.FindProperty("xRect");
        yRectProperty = serializedObject.FindProperty("yRect");
        radiusProperty = serializedObject.FindProperty("radius");

        // Cerca in Asset tutti i prefab ("t:prefab") nella cartella Prefabs
        string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs/Rooms" });
        // Ritorna i percorsi della relativa cartella Prefabs
        var paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        // Ritorna gli assets di uno specifico tipo al percorso specificato
        prefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();

        selectedPrefab = new bool[prefabs.Length];

        SceneView.duringSceneGui += DuringSceneGUI;
        Door.OnDoorFounded += AssignCollision;
    }

    private void OnDisable()
    {
        SceneView.duringSceneGui -= DuringSceneGUI;
        Door.OnDoorFounded -= AssignCollision;
    }

    private void OnGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(xRectProperty);
        EditorGUILayout.PropertyField(yRectProperty);
        EditorGUILayout.PropertyField(radiusProperty);

        if (serializedObject.ApplyModifiedProperties())
        {
            SceneView.RepaintAll();
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            GUI.FocusControl(null);
            Repaint();
        }
    }

    void DuringSceneGUI(SceneView sceneView)
    {
        DrawPrefabsIconsOnViewport();

        if (Event.current.type == EventType.MouseMove)
        {
            sceneView.Repaint();
        }

        if (serializedObject.ApplyModifiedProperties())
        {
            Repaint();
            
        }

        if (TryRaycastFromCamera())
        {
            if (Event.current.type == EventType.Repaint)
            {
                DrawBrush();
                DrawPrefabPreview();
            }
        }

        if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyDown)
        {
            SpawnPrefab();
        }

        if(Event.current.keyCode == KeyCode.LeftShift && Event.current.type == EventType.KeyDown)
        {
            RotatePrefab();
        }

        if(Event.current.keyCode == KeyCode.G && Event.current.type == EventType.KeyDown && posToSpawn != Vector3.zero)
        {
            SnapRoom();
            anyDoorFounded = true;
        }
        if (Event.current.keyCode == KeyCode.G && Event.current.type == EventType.KeyUp)
        {
            anyDoorFounded = false;
            posToSpawn = Vector3.zero;
        }
    }

    private bool TryRaycastFromCamera()
    {
        ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            return true;
        }

        return false;
    }

    private void DrawPrefabPreview()
    {
        if (prefabToSpawn == null)
        {
            isInPrefabPreviewe = false;
            return;
        }

        isInPrefabPreviewe = true;

        Matrix4x4 poseToWorldMtx = Matrix4x4.TRS(hit.point, Quaternion.identity, Vector3.one);
        MeshFilter[] filters = prefabToSpawn.GetComponentsInChildren<MeshFilter>();

        foreach (MeshFilter filter in filters)
        {
            Matrix4x4 childToPose = filter.transform.localToWorldMatrix;
            Matrix4x4 childToWorldMtx = poseToWorldMtx * childToPose;
            Mesh mesh = filter.sharedMesh;
            Material mat = filter.GetComponent<MeshRenderer>().sharedMaterial;
            mat.SetPass(0);

            if (!spawned)
            {
                obj = (GameObject)PrefabUtility.InstantiatePrefab(prefabToSpawn);
                spawned = true;
            }
            if(obj != null)
            {
                if (hit.point != null && !anyDoorFounded)
                {
                    hit.point -= new Vector3(0, hit.point.y, 0);
                    obj.transform.SetPositionAndRotation(hit.point, Quaternion.Euler(prefabToSpawn.transform.eulerAngles));
                }
            }

            //Graphics.DrawMeshNow(mesh, childToWorldMtx);
        }
    }

    private void SpawnPrefab()
    {
        GameObject thingToSpawn = (GameObject)PrefabUtility.InstantiatePrefab(prefabToSpawn);
        if(anyDoorFounded)
        {
            thingToSpawn.transform.SetPositionAndRotation(posToSpawn, rot);
        }
        else
        {
            thingToSpawn.transform.SetPositionAndRotation(hit.point, rot);
        }
        
        Undo.RegisterCreatedObjectUndo(thingToSpawn, "Object Spawn");
        thingToSpawn.TryGetComponent(out Room room);

        Door[] doors = room.GetComponentsInChildren<Door>();

        foreach (Door door in doors)
        {
            door.canTryOverlap = false;
        }
        anyDoorFounded = false;
        posToSpawn = Vector3.zero;
        timeRotated = 0;
    }

    private void RotatePrefab()
    {
        prefabToSpawn.transform.localEulerAngles += new Vector3(0, 90, 0);
        rot = Quaternion.Euler(prefabToSpawn.transform.eulerAngles);
        timeRotated++;
        OnRoomRotated?.Invoke(timeRotated);
    }

    void DrawPrefabsIconsOnViewport()
    {
        Handles.BeginGUI();

        Rect rect = new Rect(xRectProperty.floatValue, yRectProperty.floatValue, 50, 50);

        for (int i = 0; i < prefabs.Length; i++)
        {
            Texture icon = AssetPreview.GetAssetPreview(prefabs[i]);

            EditorGUI.BeginChangeCheck();
            selectedPrefab[i] = GUI.Toggle(rect, selectedPrefab[i], new GUIContent(icon));

            if (EditorGUI.EndChangeCheck())
            {
                prefabToSpawn = null;
                DestroyImmediate(obj);  

                for (int j = 0; j < prefabs.Length; j++)
                {
                    if (selectedPrefab[j])
                    {
                        rot = Quaternion.identity;
                        prefabToSpawn = prefabs[j];
                        prefabToSpawn.transform.rotation = rot;
                        spawned = false;
                    }
                }
            }
            rect.y += rect.height + 2;
        }

        Handles.EndGUI();
    }

    void DrawBrush()
    {
        Handles.color = Color.green;
        Handles.DrawWireDisc(hit.point, hit.normal, radius);;
        Handles.DrawLine(hit.point, hit.point + Vector3.forward * 3);
        Handles.DrawLine(hit.point, hit.point + Vector3.back * 3);
    }
    
    private void AssignCollision(Vector3 position)
    {
        posToSpawn = position;
    }

    private void SnapRoom()
    {
        if(obj != null)
        obj.transform.position = posToSpawn;
    }
}

