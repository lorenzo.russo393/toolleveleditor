using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;

[ExecuteAlways]
public class Door : MonoBehaviour
{
    public static Action<Vector3> OnDoorFounded;

    Vector3 dir;
    [SerializeField] LayerMask doorMask;
    public bool canTryOverlap = true;
    public bool doorFounded = false;

    Vector3 center;
    public Vector3 doorForward;
    private float offset;
    public Vector3 outPos;
    private Vector3 doorSnapPos;
    public BoxCollider otherDoor;

    private void OnEnable()
    {
        dir = transform.rotation * Quaternion.Euler(0, 90, 0) * Vector3.back;
    }

    private void Update()
    {
        Collider[] overlaps = Physics.OverlapSphere(transform.position + dir, 0.5f, doorMask);

        dir = transform.rotation * Quaternion.Euler(0, 90, 0) * Vector3.back;
        if (canTryOverlap && overlaps.Length > 0)
        {

            foreach (BoxCollider o in overlaps)
            {
                if (o.GetComponent<Door>() != this)
                {
                    Debug.Log(o.gameObject);

                    otherDoor = o;
                    center = otherDoor.transform.position + otherDoor.transform.TransformVector(otherDoor.center);

                    if(Mathf.Abs(otherDoor.size.x) - Mathf.Abs(otherDoor.size.z) >= 0)
                    {
                        doorForward = otherDoor.transform.forward;
                        offset = otherDoor.size.z;
                    }
                    else
                    {
                        doorForward = otherDoor.transform.right;
                        offset = otherDoor.size.x;
                    }

                    doorForward *= -1;
                    doorForward *= 5.5f;

                    outPos = center + doorForward * offset / 2;
                    doorSnapPos = outPos - otherDoor.transform.TransformVector(new Vector3(0, Mathf.Abs(otherDoor.size.y) / 2, 0));
                    doorSnapPos.y = 0;
                    OnDoorFounded?.Invoke(doorSnapPos);
                    doorFounded = true;
                }
                
            }
        }
        else
        {
            doorFounded = false;
        }
    }

    private void OnDrawGizmos()
    {
        if (canTryOverlap)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position + dir, 0.5f);

            Gizmos.color = Color.yellow;
        }
    }
}
